FROM node:alpine as base
WORKDIR /data
RUN apk add --no-cache tini
RUN npm install http-server -g

FROM base as builder
COPY ./Frontend/ .
RUN npm install
RUN npm run build

FROM base as final
COPY run.sh run.sh
COPY Backend Backend
RUN npm --prefix /data/Backend install
COPY --from=builder /data/dist /data/Frontend
ENTRYPOINT ["/sbin/tini", "--"]
WORKDIR /data/Backend
CMD ["/data/run.sh"]
