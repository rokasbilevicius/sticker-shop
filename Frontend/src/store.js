import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    title: 'Please create your sticker here',
    products: [
    ],
  },
  getters: {
    quantity: state => state.products.length,
  },
  mutations: {
    ADD_TO_CART: (state, product) => {
      state.products.push(product);


    },
    REMOVE_FROM_CART: (state, product) => {
      state.products.splice(product, 1);
    },
    REMOVE_ALL: (state) => {
      state.products = [];
    },
  },
  actions: {
    removeProduct: (context, product) => {
      context.commit('REMOVE_FROM_CART', product);
    },
    removeAll({ commit }) {
      return new Promise((resolve) => {
        setTimeout(() => {
          commit('REMOVE_ALL');
          resolve();
        }, 1500);
      });
    },
  },
});
