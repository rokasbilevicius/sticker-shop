// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Buefy from 'buefy';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import Vuelidate from 'vuelidate';
import VueNumberInput from '@chenfengyuan/vue-number-input';
import 'buefy/lib/buefy.css';
import "vue-material-design-icons/styles.css";
import VueStripeCheckout from 'vue-stripe-checkout';
import store from './store';
import App from './App';
import Cart from './components/Cart';
import Start from './components/Start';
import Create from './components/Create';
import StickerEmoji from 'vue-material-design-icons/StickerEmoji';
import CartIcon from 'vue-material-design-icons/Cart';
import HomeIcon from 'vue-material-design-icons/Home';

Vue.config.productionTip = false;
Vue.use(Buefy);
Vue.use(Vuex);
// Register components and services
Vue.use(VueRouter);
Vue.use(Vuelidate);
Vue.component('sticker-emoji', StickerEmoji);
Vue.component('cart-icon', CartIcon);
Vue.component('home-icon', HomeIcon);
Vue.component(VueNumberInput.name, VueNumberInput);

// Register routes
const routes = [
  { name: 'start', path: '/', component: Start},
  { name: 'create', path: '/create', component: Create },
  { name: 'cart', path: '/cart', component: Cart },
];

const router = new VueRouter({
  routes,
});
export const serverBus = new Vue();

const stripeOptions = {
  key: 'pk_test_jkeHKnP1g3XMqtdfyDCFczNj',
  image: 'https://vutu.re/_images/icon-payment-1.png',
  locale: 'US',
  currency: 'USD',
  billingAddress: true,
  shippingAddress: true,
  panelLabel: 'Pay {{amount}}',
};

Vue.use(VueStripeCheckout, stripeOptions);

/* eslint-disable no-new */
new Vue({
  store,
  router,
  data() {
    return {
      order_details: {},
    };
  },
  render: h => h(App),
}).$mount('#app');
