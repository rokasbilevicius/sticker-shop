const fs = require('fs');
const stripTags = require('striptags');

function sendEmails(mailgun, companyEmail, req, charge, orderArray) {
  // const orderArr = Object.values(charge.metadata);
  const addresses = req.body.addresses;
  const spanRight = '<span style="float:right;">';
  const bodyDiv = '<div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">'


  let stickerPlural;
  const orderLenght = orderArray.length;
  orderLenght > 1 ? stickerPlural = 'stickers' : stickerPlural = 'sticker';

  let stickerText = '';
  let i = 1;
  orderArray.forEach(function (value) {
    stickerText = stickerText + bodyDiv + '<b>Sticker nr. ' + i + ' Quantity: ' + value[1] + '</b><br>'
      + bodyDiv + value[0] + '</div></div><br>';
    i++;
  });
  stickerText = `<br>${stickerText}`;

  const shippingPlaceholder =
    bodyDiv + 'Recipients name: ' + spanRight + stripTags(addresses.shipping_name) + '</span></div>' +
    bodyDiv + 'Address: ' + spanRight + stripTags(addresses.shipping_address_line1) + '</span></div>' +
    bodyDiv + 'ZIP: ' + spanRight + stripTags(addresses.shipping_address_zip) + '</span></div>' +
    bodyDiv + 'City: ' + spanRight + stripTags(addresses.shipping_address_city) + '</span></div>' +
    bodyDiv + 'State: ' + spanRight + stripTags(addresses.shipping_address_state) + '</span></div>' +
    bodyDiv + 'Country: ' + spanRight + stripTags(addresses.shipping_address_country) + '</span></div>';

  let customerContent = fs.readFileSync('./templates/order.html', 'utf-8');
  let companyContent = customerContent;

  customerContent = customerContent.replace(/{{introductionPlaceholder}}/, `You have ordered <b>${orderLenght}</b> ${stickerPlural}!`);
  customerContent = customerContent.replace(/{{orderIdPlaceholder}}/, charge.id);
  customerContent = customerContent.replace(/{{greetingPlaceholder}}/, 'Your order is on the way!');
  customerContent = customerContent.replace(/{{introPlaceholder}}/, `${stripTags(addresses.billing_name)}, thanks for choosing our services!<br>Please find your order's details below...`);
  customerContent = customerContent.replace(/{{quantityPlaceholder}}/, `You have ordered <b>${orderLenght}</b> ${stickerPlural}:`);
  customerContent = customerContent.replace(/{{stickerTextPlaceholder}}/, stickerText);
  customerContent = customerContent.replace(/{{shippingPlaceholder}}/, shippingPlaceholder);

  companyContent = companyContent.replace(/{{introductionPlaceholder}}/, `Customer has ordered <b>${orderLenght}</b> ${stickerPlural}!`);
  companyContent = companyContent.replace(/{{orderIdPlaceholder}}/, charge.id);
  companyContent = companyContent.replace(/{{greetingPlaceholder}}/, 'New order has been placed!');
  companyContent = companyContent.replace(/{{introPlaceholder}}/, 'Please print and ship the order.<br>Order details below...');
  companyContent = companyContent.replace(/{{quantityPlaceholder}}/, `Customer has ordered <b>${orderLenght}</b> ${stickerPlural}:`);
  companyContent = companyContent.replace(/{{stickerTextPlaceholder}}/, stickerText);
  companyContent = companyContent.replace(/{{shippingPlaceholder}}/, shippingPlaceholder);

  // send email to customer
  let emailData = {
    from: companyEmail,
    to: req.body.token_from_stripe.email,
    subject: 'Sticker-shop - order confirmation - ' + charge.id,
    html: customerContent
  };
  mailgun.messages().send(emailData);

  // send email to supplier
  emailData['to'] = companyEmail;
  emailData['from'] = req.body.token_from_stripe.email,
  emailData['subject'] = `Sticker-shop - new order: ${charge.id}`;
  emailData['html'] = companyContent;
  mailgun.messages().send(emailData);
}

module.exports = sendEmails;
