require('dotenv').config();

var express = require('express'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  cors = require('cors'),
  app = express();
// ENVIRONMENT CONFIG
var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var port = process.env.PORT || 3000;
var router = express.Router();
// EXPRESS CONFIG
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(methodOverride());
app.use(express.static(__dirname + '/public'));
// stripe config
var stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
// mailgun config
var mailgun = require('mailgun-js')({
  apiKey: process.env.MAILGUN_API_KEY,
  domain: process.env.MAILGUN_DOMAIN,
});
// import components
const sendEmails = require('./components/sendEmails');
const stripTags = require('striptags');
// ROUTES
router.post('/charge', function(req, res){
  this.emailObj = [];
  var metadataObj = {};
  const order = Object.values(req.body.metadata);
  var i = 0;
  order.forEach(function (stickerValue){
    const textName = 'sticker_' + i;
    const quanName = 'quantity_' + i;
    const textArray = Object.values(stickerValue[0]);
    var text = '';
    var emailtext = '';
    textArray.forEach(function (textValue){
      if (textValue !== '') {
        text = text + stripTags(textValue) + '^p';
        emailtext = emailtext + stripTags(textValue) + '<br>';
      }
    });
    const quantity = stickerValue[1];
    var emailSticker = [];
    emailSticker.push(emailtext);
    emailSticker.push(quantity);
    this.emailObj.push(emailSticker);
    metadataObj[textName] = text;
    metadataObj[quanName] = quantity;
    i++;
  });
  console.log(metadataObj);

  const newCharge = {
    amount: req.body.amount,
    currency: req.body.currency,
    source: req.body.token_from_stripe.id,
    metadata: metadataObj,
  };
  stripe.charges.create(newCharge, function(err, charge) {
    if (err){
      console.error(err);
      res.json({ error: err, charge: false });
    } else {
      sendEmails(mailgun, process.env.COMPANY_EMAIL, req, charge, this.emailObj);
      res.json({ error: false, charge: charge });
    }
  });
});

app.use('/', router);
// Start server
app.listen(port, function(){
  console.log('Server listening on port ' + port)
});
